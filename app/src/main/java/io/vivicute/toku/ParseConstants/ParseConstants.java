package io.vivicute.toku.ParseConstants;

/**
 * Created by ocabafox on 10/11/2015.
 */
public final class ParseConstants {
    //class
    public static final String CLASS_MESSAGE = "Messages";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_FRIENDS_RELATION = "friend_relation";
    public static final String KEY_RECIPIENT_IDS = "recipientIds";
    public static final String KEY_SENDER_ID = "senderId";
    public static final String KEY_SENDER_NAME = "senderName";
    public static final String KEY_FILE = "file";
    public static final String KEY_FILE_TYPE = "fileType";
    public static final String KEY_CREATED_AT = "createdAt";

    public static final String TYPE_IMAGE = "image";
    public static final String TYPE_VIDEO = "video";
}
