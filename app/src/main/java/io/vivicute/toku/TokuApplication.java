package io.vivicute.toku;
import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.PushService;

import io.vivicute.toku.ParseConstants.ParseConstants;

/**
 * Created by Dj on 10/7/2015.
 */
public class TokuApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);

        Parse.enableLocalDatastore(this);
        Parse.initialize(this, getString(R.string.app_key), getString(R.string.client_key));

        ParseInstallation.getCurrentInstallation().saveInBackground();
    }

    public static void updateParseInstallation(ParseUser user){
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(ParseConstants.KEY_USER_ID, user.getObjectId());
        installation.saveInBackground();
    }
}
