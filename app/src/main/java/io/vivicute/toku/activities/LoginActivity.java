package io.vivicute.toku.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.vivicute.toku.R;
import io.vivicute.toku.TokuApplication;

/**
 * Created by Dj on 10/7/2015.
 */
public class LoginActivity extends AppCompatActivity{
    @Bind(R.id.edt_username)
    EditText mEditUsername;
    @Bind(R.id.edt_password)
    EditText mEditPassword;
    @Bind(R.id.progressBar)
    ProgressBar mProgress;
    @Bind(R.id.txt_signup)
    TextView mTextSignup;
    @Bind(R.id.btn_login)
    Button mBtnLogin;

    private static String username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.txt_signup)
    public void onSignupClicked(){
        Intent intent = new Intent(this,SignupActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.btn_login)
    public void onLoginClicked() {
        username = mEditUsername.getText().toString();
        password = mEditPassword.getText().toString();

        if (verifyLoginInputs(username, password)){
            mProgress.setVisibility(View.VISIBLE);
            mBtnLogin.setVisibility(View.GONE);
            mTextSignup.setVisibility(View.GONE);
            ParseUser.logInInBackground(username, password, new LogInCallback() {
                @Override
                public void done(ParseUser parseUser, ParseException e) {
                    mProgress.setVisibility(View.GONE);
                    mBtnLogin.setVisibility(View.VISIBLE);
                    mTextSignup.setVisibility(View.VISIBLE);

                    if (e == null) {
                        TokuApplication.updateParseInstallation(parseUser);

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else
                        showErrorDialog(getString(R.string.error_login_invalid));
                }
            });
        }
        else
            showErrorDialog(getString(R.string.error_login_empty));

    }

    private boolean verifyLoginInputs(String username, String password) {
        if (username.isEmpty() || password.isEmpty())
            return false;
        else
            return true;
    }

    private void showErrorDialog(String errorMsg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setMessage(errorMsg)
                .setTitle(R.string.error_title)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }
}
