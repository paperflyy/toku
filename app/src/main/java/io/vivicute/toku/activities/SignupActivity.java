package io.vivicute.toku.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.vivicute.toku.R;
import io.vivicute.toku.TokuApplication;

/**
 * Created by Dj on 10/7/2015.
 */
public class SignupActivity extends AppCompatActivity {
    @Bind(R.id.edt_username)
    EditText mEditUsername;
    @Bind(R.id.edt_password)
    EditText mEditPassword;
    @Bind(R.id.edt_email)
    EditText mEditEmail;
    @Bind(R.id.progressBar)
    ProgressBar mProgress;

    private static String username, password, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.txt_login)
    public void onLoginClicked(){
        Intent intent = new Intent(this,LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.btn_signup)
    public void onSignupClicked() {
        username = mEditUsername.getText().toString();
        password = mEditPassword.getText().toString();
        email = mEditEmail.getText().toString();

        if (verifySignupInputs(username, password, email)){
            mProgress.setVisibility(View.VISIBLE);

            ParseUser newUser = new ParseUser();
            newUser.setUsername(username);
            newUser.setPassword(password);
            newUser.setEmail(email);
            newUser.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    mProgress.setVisibility(View.GONE);

                    if (e == null) {
                        TokuApplication.updateParseInstallation(
                                ParseUser.getCurrentUser());

                        Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else
                        showErrorDialog(e.getMessage());
                }
            });
        }
        else
            showErrorDialog(getString(R.string.error_signup_empty));

    }

    private boolean verifySignupInputs(String username, String password, String email) {
        if (username.isEmpty() || password.isEmpty() || email.isEmpty())
            return false;
        else
            return true;
    }

    private void showErrorDialog(String errorMsg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
        builder.setMessage(errorMsg)
                .setTitle(R.string.error_title)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }
}
