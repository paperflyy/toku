package io.vivicute.toku.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.vivicute.toku.ParseConstants.ParseConstants;
import io.vivicute.toku.R;
import io.vivicute.toku.recyclerView.FriendsAdapter;
import io.vivicute.toku.recyclerView.FriendsModel;

/**
 * Created by Dj on 10/8/2015.
 */
public class FriendsFragment extends Fragment {
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    public static final String TAG = FriendsFragment.class.getSimpleName();

    protected ParseRelation<ParseUser> mFriendsRelation;
    protected ParseUser mCurrentUser;
    protected List<ParseUser> mFriends;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.user_grid, container, false);
        ButterKnife.bind(this, rootView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        mCurrentUser = ParseUser.getCurrentUser();
        mFriendsRelation = mCurrentUser.getRelation(ParseConstants.KEY_FRIENDS_RELATION);

        getActivity().setProgressBarIndeterminateVisibility(true);

        ParseQuery<ParseUser> query = mFriendsRelation.getQuery();
        query.addAscendingOrder(ParseConstants.KEY_USERNAME);
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> friends, ParseException e) {
                getActivity().setProgressBarIndeterminateVisibility(false);

                if (e == null) {
                    mFriends = friends;
                    ArrayList<FriendsModel> itemData = new ArrayList<>();

                    int i = 0;
                    for (ParseUser user : mFriends) {
                        itemData.add(new FriendsModel(user.getUsername(), user.getEmail()));
                        i++;
                    }

                        FriendsAdapter mAdapter = new FriendsAdapter(itemData);

                    if(recyclerView.getAdapter() == null)
                        recyclerView.setAdapter(mAdapter);
                    else
                        ((FriendsAdapter) recyclerView.getAdapter()).refill((ArrayList) mFriends);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());

                } else {
                    Log.e(TAG, e.getMessage());
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(e.getMessage())
                            .setTitle(R.string.error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
    }
}
