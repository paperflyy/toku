package io.vivicute.toku.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.vivicute.toku.ParseConstants.ParseConstants;
import io.vivicute.toku.R;
import io.vivicute.toku.receiver.IncomingReceiver;
import io.vivicute.toku.recyclerView.FriendsModel;
import io.vivicute.toku.recyclerView.InboxAdapter;
import io.vivicute.toku.recyclerView.InboxModel;

/**
 * Created by Dj on 10/8/2015.
 */
public class InboxFragment extends Fragment {
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.img_nomsg)
    ImageView imgNomsg;
    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private static boolean mReceiverIsRegistered;

//    protected List<ParseObject> mMessages;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_inbox, container, false);
        ButterKnife.bind(this, rootView);
        swipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
        swipeRefreshLayout.setColorSchemeResources(R.color.accent);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mReceiverIsRegistered) {
            getActivity().unregisterReceiver(updateReceiver);
            mReceiverIsRegistered = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mReceiverIsRegistered) {
            getActivity().registerReceiver(updateReceiver,
                    new IntentFilter(IncomingReceiver.BROADCAST_PUSH));
            mReceiverIsRegistered = true;
        }

        getActivity().setProgressBarIndeterminate(true);

        retrieveMessages();
    }

    private BroadcastReceiver updateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            retrieveMessages();
        }
    };

    private void retrieveMessages() {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(ParseConstants.CLASS_MESSAGE);
        query.whereEqualTo(ParseConstants.KEY_RECIPIENT_IDS, ParseUser.getCurrentUser().getObjectId());
        query.addDescendingOrder(ParseConstants.KEY_CREATED_AT);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                getActivity().setProgressBarIndeterminate(false);
                if (e == null) {
                    InboxAdapter mAdapter = new InboxAdapter(list);
                    recyclerView.setAdapter(mAdapter);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    if(mAdapter.getItemCount()!=0)
                        imgNomsg.setVisibility(View.GONE);
                    else
                        imgNomsg.setVisibility(View.VISIBLE);
                } if(swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    protected SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            retrieveMessages();
        }
    };
}
