package io.vivicute.toku.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import io.vivicute.toku.R;

/**
 * Created by Dj on 10/19/2015.
 */
public class IncomingReceiver extends ParsePushBroadcastReceiver {
    public static final String BROADCAST_PUSH = "io.vivicute.toku.receiver.push";
    private static final int NOTIFICATION_ID = 1;
    public static int numMessages = 0;


    protected void onPushReceive(Context mContext, Intent intent) {
        mContext.sendBroadcast(new Intent(BROADCAST_PUSH));
    }
}