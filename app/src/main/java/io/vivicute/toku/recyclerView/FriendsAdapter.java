package io.vivicute.toku.recyclerView;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.vivicute.toku.R;
import io.vivicute.toku.utils.MD5Util;

/**
 * Created by ocabafox on 10/11/2015.
 */
public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {
    private ArrayList<FriendsModel> itemsData;
    Context context;

    public FriendsAdapter(ArrayList<FriendsModel> itemsData) {
        this.itemsData = itemsData;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FriendsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_friends, parent,false);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        try {
            viewHolder.name.setText(itemsData.get(position).getName());
            String email = itemsData.get(position).getEmail();
            if (!email.equals("")) {
                String hash = MD5Util.md5Hex(email.toLowerCase());
                String gravatarUrl = "http://www.gravatar.com/avatar/" + hash
                        + "?s=204&d=404";
                Picasso.with(context).load(gravatarUrl).placeholder(R.drawable.avatar_empty).into(viewHolder.imgUserAvatar);
            }
        }catch (ClassCastException e){}
    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.editFriends_ID)
        TextView name;
        @Bind(R.id.img_user_avatar)
        ImageView imgUserAvatar;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
            context = itemLayoutView.getContext();
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public void refill(ArrayList<ParseUser> users){
        itemsData.clear();
        itemsData.addAll((ArrayList) users);
        notifyDataSetChanged();
    }
}
