package io.vivicute.toku.recyclerView;

/**
 * Created by ocabafox on 10/11/2015.
 */
public class FriendsModel {
    private String name;
    private String email;

    public FriendsModel(String name, String email){
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail(){
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {this.email = email;}

}
