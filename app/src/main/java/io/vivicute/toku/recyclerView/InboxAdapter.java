package io.vivicute.toku.recyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.vivicute.toku.ParseConstants.ParseConstants;
import io.vivicute.toku.R;
import io.vivicute.toku.activities.ViewImageActivity;

/**
 * Created by Dj on 10/14/2015.
 */
public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.ViewHolder> {
    private static List<ParseObject> mMessages;

    public InboxAdapter(List<ParseObject> mMessages) {
        this.mMessages = mMessages;
    }

    @Override
    public InboxAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_inbox, parent, false);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        ParseObject message = mMessages.get(position);

        Date createdAt = message.getCreatedAt();
        long now = new Date().getTime();
        String convertedDate = DateUtils.getRelativeTimeSpanString(
                createdAt.getTime(),
                now,
                DateUtils.SECOND_IN_MILLIS).toString();

        if(message.getString(ParseConstants.KEY_FILE_TYPE).equals(ParseConstants.TYPE_IMAGE)){
            viewHolder.imgIcon.setImageResource(R.drawable.ic_crop_original);
        }else{
            viewHolder.imgIcon.setImageResource(R.drawable.ic_videocam);
        }
        viewHolder.txtName.setText(mMessages.get(position).getString(ParseConstants.KEY_SENDER_NAME));
        viewHolder.txtTime.setText(convertedDate);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        @Bind(R.id.text_name)
        TextView txtName;
        @Bind(R.id.img_icon)
        ImageView imgIcon;
        @Bind(R.id.text_time)
        TextView txtTime;
        private Context context;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
            context = itemView.getContext();
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ParseObject msg = mMessages.get(getPosition());
            String msgType = msg.getString(ParseConstants.KEY_FILE_TYPE);
            ParseFile file = msg.getParseFile(ParseConstants.KEY_FILE);
            Uri fileUri = Uri.parse(file.getUrl());


            if(msgType.equals(ParseConstants.TYPE_IMAGE)){
                Intent intent = new Intent(context, ViewImageActivity.class);
                intent.setData(fileUri);
                context.startActivity(intent);
            }else{
                Intent intent = new Intent(Intent.ACTION_VIEW, fileUri);
                intent.setDataAndType(fileUri, "video/*");
                context.startActivity(intent);
            }

            // Delete it!
            List<String> ids = msg.getList(ParseConstants.KEY_RECIPIENT_IDS);

            if (ids.size() == 1) {
                // last recipient - delete the whole thing!
                msg.deleteInBackground();
            }
            else {
                // remove the recipient and save
                ids.remove(ParseUser.getCurrentUser().getObjectId());

                ArrayList<String> idsToRemove = new ArrayList<String>();
                idsToRemove.add(ParseUser.getCurrentUser().getObjectId());

                msg.removeAll(ParseConstants.KEY_RECIPIENT_IDS, idsToRemove);
                msg.saveInBackground();
            }

        }
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }
}
