package io.vivicute.toku.recyclerView;

/**
 * Created by Dj on 10/14/2015.
 */
public class InboxModel {
    private String msg;

    public InboxModel(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
